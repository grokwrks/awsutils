<?php

$infile = file_get_contents($argv[1]);

$in = json_decode($infile);

$out = new stdClass();

$out->Comment = "Converted from " . $argv[1] . " at " . date("Y-m-d H:i:s");

$changes = array();

$ignorerecs = array(
        "SOA",
        "NS",
);

foreach ($in->ResourceRecordSets as $record) {

        if (!in_array($record->Type, $ignorerecs)) {
                $chgrec = new stdClass();
                $chgrec->Action = "CREATE";
                $chgrec->ResourceRecordSet = $record;
                $changes[] = $chgrec;
        }
}


$out->Changes = $changes;

$json = json_encode($out);

$newfile = $argv[1] . "." . date("YmdHis");

file_put_contents($newfile,$json);

