<?php

$awsold = "aws";
$awsnew = "aws --profile daprod";

$zone = $argv[1];

$zonelistoldjson = `$awsold  route53 list-hosted-zones`;
$zonelistold = json_decode($zonelistoldjson);

$zonelistnewjson = `$awsnew  route53 list-hosted-zones`;
$zonelistnew = json_decode($zonelistnewjson);

$zoneoldid = findzoneid($zone, $zonelistold);

echo "Old Zone Id = $zoneoldid\n";

$zonenewid = findzoneid($zone, $zonelistnew);
if ($zonenewid) {
        echo "New Zone Id = $zonenewid\n";
} else {
        echo "Createing new Zone....\n";
        $ref = date("YmdHis");
        $newzonejson = `$awsnew route53 create-hosted-zone --name $zone --caller-reference $ref`;
        $newzone = json_decode($newzonejson);
        $zonenewid = $newzone->HostedZone->Id;
        echo "New Zone Id = $zonenewid\n";
}

$oldzonejson = `$awsold route53 list-resource-record-sets --hosted-zone-id $zoneoldid`;
$oldzone = json_decode($oldzonejson);

$newzonejson = `$awsnew route53 list-resource-record-sets --hosted-zone-id $zonenewid`;
$newzone = json_decode($newzonejson);


$out = new stdClass();

$out->Comment = "Updated from " . $argv[1] . " at " . date("Y-m-d H:i:s");

$changes = array();

$ignorerecs = array(
        "SOA",
        "NS",
);

foreach ($oldzone->ResourceRecordSets as $record) {

        if (!in_array($record->Type, $ignorerecs)) {
                $chgrec = new stdClass();
                if (findnameinset($record->Name,$record->Type,$newzone)) {
                        $chgrec->Action = "UPSERT";
                } else {
                        $chgrec->Action = "CREATE";
                }
                $chgrec->ResourceRecordSet = $record;
                $changes[] = $chgrec;
        }
}


$out->Changes = $changes;
$filename = "/tmp/$zone.migration." . date("YmdHis"); 

file_put_contents($filename,json_encode($out));

echo `$awsnew route53 change-resource-record-sets --hosted-zone-id $zonenewid --change-batch file://$filename`;


function findzoneid($zone,$set) {

        foreach ($set->HostedZones as $record) {
                if ($record->Name == $zone . ".") {
                        return $record->Id;
                }
        }
        return false;
}

function findnameinset($name,$type,$set) {
        foreach ($set->ResourceRecordSets as $record) {
                if ($record->Type = $type && $record->Name == $name) {
                        return true;
                }
        }
        return false;
}

